Rails.application.routes.draw do
  devise_for :users, controllers: {
  	confirmations: 'users/confirmations',
  	passwords: 'users/passwords',
  	registrations: 'users/registrations',
  	sessions: 'users/sessions',
  	unlocks: 'users/unlocks'
  }
  # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
  #resources :products
  get '/products', to: 'products#index', as: :products
  get '/product/:id', to: 'products#show', as: :product
  get '/dashboard/products', to: 'products#manage_products', as: :manage_products
  get '/dashboard/products/new', to: 'products#new', as: :new_product
  post '/dashboard/products', to: 'products#create', as: :create_product
  get '/dashboard/product/:id/edit', to: 'products#edit', as: :edit_product
  put '/dashboard/product/:id', to: 'products#update', as: :update_product
  delete '/dashboard/product/:id', to: 'products#destroy', as: :delete_product

  resource :user do
    resources :shipping_addresses
  end

  resource :cart
  post 'cart_items' => 'carts#batch_process', as: :cart_item_batch_process

  get 'order/:id/shipping-address' => 'orders#fill_shipping_address', as: :fill_order_shipping_address
  post 'order/:id/shipping-address' => 'orders#save_shipping_address', as: :save_order_shipping_address
  get 'orders' => 'orders#index', as: :orders
  get '/dashboard/orders' => 'orders#manage_orders', as: :manage_orders
  match '/dashboard/order/:id/update_status' => 'orders#update_status', via: [:get, :put], as: :update_order_status

  root to: "products#index"
end
