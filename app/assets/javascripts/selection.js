var SimpleSelection = function(options) {
	var defaultOptions = {
		itemSelector: '.select-item',
		allSelector: 'input[name="select-all"]'
	};
	if (options) {
		this.options = $.extend(defaultOptions, options);
	} else {
		this.options = defaultOptions;
	}
};

$.extend(SimpleSelection.prototype, {
	selectAll: function() {
		$(this.options.itemSelector).prop('checked', true);
	},

	deselectAll: function() {
		$(this.options.itemSelector).prop('checked', false);
	},

	getSelectedItemValues: function() {
		return $(this.options.itemSelector.concat(':checked')).map(function() {
			return $(this).val();
		}).get();
	}
});