class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :trackable, :validatable, :timeoutable

  validates :firstname, presence: true
  validates :lastname, presence: true
  validates :role, presence: true

  has_one :cart
  has_many :shipping_addresses
  has_many :orders

  def name
  	"#{firstname} #{lastname}"
  end

  def seller?
  	role == 'Seller'
  end

  def customer?
  	role == 'Customer'
  end
end
