class Order < ApplicationRecord
  belongs_to :user
  has_many :order_items

  # enum status: {
  # 	draft: 0,
  # 	paid: 1,
  # 	pending: 2,
  # 	confirmed: 3,
  # 	shipped: 4,
  # 	fulfilled: 5,
  # 	canceled: 6,
  # 	completed: 7
  # }
end
