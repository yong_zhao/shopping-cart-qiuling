class CreateOrders < ActiveRecord::Migration[5.1]
  def change
    create_table :orders do |t|
      t.string :status
      t.string :shipping_address
      t.float :total

      t.timestamps
    end
  end
end
